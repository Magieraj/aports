# Contributor: knuxify <knuxify@gmail.com>
# Maintainer: knuxify <knuxify@gmail.com>
pkgname=libopenmpt
pkgver=0.6.6
pkgrel=0
pkgdesc="Cross-platform library to render tracker music to a PCM audio stream"
url="https://lib.openmpt.org/libopenmpt/"
arch="all"
license="BSD-3-Clause"
makedepends="flac-dev libogg-dev pulseaudio-dev libsndfile-dev libvorbis-dev mpg123-dev portaudio-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc openmpt123:_openmpt123 openmpt123-doc:_openmpt123_doc"
source="https://lib.openmpt.org/files/libopenmpt/src/libopenmpt-$pkgver+release.autotools.tar.gz"
builddir="$srcdir/libopenmpt-$pkgver+release.autotools"

prepare() {
	default_prepare
	# Drop +release.autotools suffix from version number; otherwise pkgconf
	# files don't pass abuild's requirements
	sed -i 's/+release.autotools//' "$builddir"/configure
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

_openmpt123() {
	pkgdesc="Command-line module file player"
	amove usr/bin/openmpt123
}

_openmpt123_doc() {
	pkgdesc="Command-line module file player (documentation)"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/../$pkgname-doc/usr/share/man "$subpkgdir"/usr/share
}

sha512sums="
b634c556f13dc51d1008f4216936a9b7cab25a6fb0d5218da0b692ec848de21905ed1981223ac9ecdebea9ce6c5376e91ff92e1655dd0be491fce0114e3230bf  libopenmpt-0.6.6+release.autotools.tar.gz
"
