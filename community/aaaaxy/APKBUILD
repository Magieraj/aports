# Contributor: Rudolf Polzer <divVerent@gmail.com>
# Maintainer: Rudolf Polzer <divVerent@gmail.com>
pkgname=aaaaxy
pkgver=1.3.43
pkgrel=0
pkgdesc="A nonlinear puzzle platformer taking place in impossible spaces"
url="https://divVerent.github.io/aaaaxy/"
arch="all !s390x !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="
	alsa-lib-dev
	go
	libx11-dev
	libxcursor-dev
	libxinerama-dev
	libxi-dev
	libxrandr-dev
	mesa-dev
"
checkdepends="xvfb-run mesa-dri-gallium"
source="
	https://github.com/divVerent/aaaaxy/archive/v$pkgver/aaaaxy-$pkgver.tar.gz
	https://github.com/divVerent/aaaaxy/releases/download/v$pkgver/sdl-gamecontrollerdb-for-aaaaxy-v$pkgver.zip
"
options="net"  # Needed for go mod download.

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	mv "$srcdir"/third_party/SDL_GameControllerDB/assets/input/* \
		third_party/SDL_GameControllerDB/assets/input/
	go mod download
}

build() {
	export AAAAXY_BUILD_USE_VERSION_FILE=true
	make BUILDTYPE=release
}

check() {
	xvfb-run sh scripts/regression-test-demo.sh aaaaxy \
		"on track for Any%, All Paths and No Teleports" \
		./aaaaxy benchmark.dem
}

package() {
	install -Dm644 io.github.divverent.aaaaxy.metainfo.xml \
		"$pkgdir"/usr/share/metainfo/io.github.divverent.aaaaxy.metainfo.xml
	install -Dm644 aaaaxy.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/aaaaxy.png
	install -Dm644 aaaaxy.desktop \
		"$pkgdir"/usr/share/applications/aaaaxy.desktop
	install -Dm755 aaaaxy \
		"$pkgdir"/usr/bin/aaaaxy
}

sha512sums="
5e790bc579587f647ba3c592ed3cd5a576c58f2798de2672dde7327ecbfd263e51c0e3a6172da3e8952c4f6e4bf7acdcc6f312f71bb488b63839b0232a58ea1e  aaaaxy-1.3.43.tar.gz
c7547816304385b0c4229ea8ae07257df7446392021bff7ffecc432b84ff04794f1cf81351cf97aae0aa825d2766e99030588801023c713abf8718f7903638b7  sdl-gamecontrollerdb-for-aaaaxy-v1.3.43.zip
"
