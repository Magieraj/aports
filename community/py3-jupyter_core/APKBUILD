# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-jupyter_core
pkgver=5.1.0
pkgrel=0
pkgdesc="Core Jupyter functionality"
url="https://github.com/jupyter/jupyter_core"
arch="noarch"
license="BSD-3-Clause"
depends="py3-traitlets py3-platformdirs"
makedepends="py3-hatchling py3-gpep517"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://github.com/jupyter/jupyter_core/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/jupyter_core-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest \
		--deselect jupyter_core/tests/test_command.py::test_not_on_path \
		--deselect jupyter_core/tests/test_command.py::test_path_priority \
		--deselect jupyter_core/tests/test_paths.py::test_jupyter_path_prefer_env \
		--deselect jupyter_core/tests/test_paths.py::test_jupyter_path_user_site \
		--deselect jupyter_core/tests/test_paths.py::test_jupyter_path_no_user_site \
		--deselect jupyter_core/tests/test_command.py::test_argv0
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/jupyter_core-$pkgver-py3-none-any.whl
}

sha512sums="
9ef706e68f5195bf5f29c015f042584fff65195a5bc760735963118bf43e6702124535be518bcc1d2da57f3bc153c571a261f3182aae3e4cf4c0eb5956dd987f  py3-jupyter_core-5.1.0.tar.gz
"
