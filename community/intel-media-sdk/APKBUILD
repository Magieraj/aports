# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=intel-media-sdk
pkgver=22.6.3
pkgrel=0
pkgdesc="Hardware-accelerated video processing on Intel integrated GPUs"
url="http://mediasdk.intel.com/"
arch="x86_64"
license="MIT"
depends="libmfx=$pkgver-r$pkgrel"
depends_dev="libva-dev"
checkdepends="gtest-dev"
makedepends="
	$depends_dev
	cmake
	libx11-dev
	ninja
	wayland-dev
	"
subpackages="$pkgname-tracer $pkgname-dev libmfx"
source="https://github.com/Intel-Media-SDK/MediaSDK/archive/intel-mediasdk-$pkgver.tar.gz
	musl-compat.patch
	"
builddir="$srcdir/MediaSDK-intel-mediasdk-$pkgver"

build() {
	# CMAKE_BUILD_TYPE=Release - do not change to None!
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DENABLE_OPENCL=ON \
		-DENABLE_X11_DRI3=ON \
		-DENABLE_WAYLAND=ON \
		-DENABLE_TEXTLOG=ON \
		-DENABLE_STAT=ON \
		-DBUILD_SAMPLES=OFF \
		-DBUILD_TOOLS=OFF \
		-DBUILD_TUTORIALS=OFF \
		-DBUILD_TESTS=$(want_check && echo ON || echo OFF) \
		-DUSE_SYSTEM_GTEST=$(want_check && echo ON || echo OFF)
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

tracer() {
	pkgdesc="Intel Media SDK Tracer"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/bin/mfx-tracer-config
	amove usr/lib/libmfx-tracer.so.*
}

libmfx() {
	pkgdesc="Intel Media SDK dispatcher library"
	depends=""

	amove usr/lib/libmfx.so.*
}

sha512sums="
bd9fb9ca5b96bb2911db7fdf2622127f0dc3e86de8dc732bc6df6f4fcb343406e3ab14ffa5ce4595ed9b3bf19a73652681137fbe5b473c812c5b74554998a2b0  intel-mediasdk-22.6.3.tar.gz
c04a538a3699a0d4a94b80e1d4b7de2e01225755b4f57ab5bfd184d97ec3315cc0721cb1854886ae5194563903c48429030eab805abe6442657abf6ee8f03f3b  musl-compat.patch
"
