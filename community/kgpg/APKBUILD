# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgpg
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi-contacts
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kgpg"
pkgdesc="A simple interface for GnuPG, a powerful encryption utility"
license="GPL-2.0-or-later"
makedepends="
	akonadi-contacts-dev
	extra-cmake-modules
	gpgme-dev
	karchive-dev
	kcodecs-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgpg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kgpg-import fails too often
	# kgpg-encrypt and kgpg-export are broken
	# del-key fails randomly
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kgpg-(import|encrypt|export|del-key)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4b6a332345eb8f90ee0c677121fdf51ce8630119a10ca33cfc4381d1466292259017bd19263b8cf3d71642e155326468d13d1550792bc77acdf4c2ff1ce464a3  kgpg-22.12.0.tar.xz
"
