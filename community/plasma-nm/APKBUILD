# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-nm
pkgver=5.26.4
pkgrel=0
pkgdesc="Plasma applet written in QML for managing network connections"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-or-later"
depends="
	kirigami2
	networkmanager
	"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kservice-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	mobile-broadband-provider-info
	modemmanager-qt-dev
	networkmanager-qt-dev
	plasma-framework-dev
	prison-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-nm-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-mobile"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_MOBILE=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

mobile() {
	pkgdesc="$pkgdesc (mobile KCM's)"
	depends="$depends $pkgname"

	amove usr/share/kpackage
	amove usr/lib/qt5/plugins/kcms
}

sha512sums="
2dfdd65d43441f0a1963c24daddb5c718c0403e58419e396d02b6b2c8c40f68d3cb66934bb5e36dbc8cb8ad3095ff95cf1f9bd879066c832dab13236991017bf  plasma-nm-5.26.4.tar.xz
"
