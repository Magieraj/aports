# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktp-common-internals
pkgver=22.12.0
pkgrel=0
pkgdesc="Library for KTp"
url="https://github.com/kde/ktp-common-internals"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="LGPL-2.1-only AND GPL-2.0-only"
depends_dev="
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kemoticons-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	knotifyconfig-dev
	kpeople-dev
	ktexteditor-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	telepathy-qt-dev
	"
makedepends="$depends_dev extra-cmake-modules doxygen"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktp-common-internals-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
04a2ae5858857e732a7931e9062229d8018f85357ddbcf75b5a257b9b1bc749f6d7d2ddc028ffceffcb0d6a1199b31d9ecc42ea4598703c572cce8f511f4930a  ktp-common-internals-22.12.0.tar.xz
"
