# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libksane
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/graphics/"
pkgdesc="An image scanning library"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	ki18n-dev
	ksanecore-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksane-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3e44d3c8f37dbfd4d65483526a412944741b30f1cdd205663c87dfce00a7e5f5e4d4cac0c014e4f48d39b681449f83daab1629f125198f79a2fe1f4c96214db6  libksane-22.12.0.tar.xz
"
